import React, {useState, useEffect} from 'react';
import { Button, Spinner } from 'react-bootstrap';
import BasicLayout from "../../layout/BasicLayout";
import { toast } from 'react-toastify';
import { getUserApi } from '../../api/user';
import { getUserTweetsApi } from '../../api/tweet';
import { useParams } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';
import BannerAvatar from '../../components/User/BannerAvatar';
import InfoUser from '../../components/User/InfoUser';
import ListTweets from '../../components/ListTweets/ListTweets';

import "./User.scss";

export default function User(props) {
  
  //const {match} = props;
  const params = useParams(); //El useParams extrae el user?._id(id del usuario logeado) del LeftMenu(linea 32) y lo retorna en este formato: {id:63d8672d6777180efb9ec6ab}, por cierto el "id" proviene del configRouting.js (path:"/:id")
  // Aparece el id del usuario que se esta visitando solo en el link: http://localhost:3000/(63dae5e698950509c140779a)<-- este es el "id" del usuario que se esta visitando  || LeftMenu(linea 30)
  const {setRefreshCheckLogin} = props;
  const [user, setUser] = useState(null);
  const [tweets, setTweets] = useState(null); //Aqui contiene los twuits del usuario(_id)
  const [page, setPage] = useState(1);
  const [loadingTweets, setLoadingTweets] = useState(false);
  const loggedUser = useAuth(); //Aqui contiene toda la informacion del "usuario logeado", para salir de duda la clave esta en App.js. || por cierto el nombre useAuth proviene de useAuth.js
  //console.log(loggedUser) //Aparece un Json con toda la informacion del usuario logeado
  //console.log(params); //ver el comentario de const params || resultado params: { id: '63d8672d6777180efb9ec6ab' }
  //console.log(tweets) //Aparece los twits del usuario(_id)
  //console.log(setRefreshCheckLogin)
  useEffect (() => {
    getUserApi(params.id) //el "id" viene de path:"/:id" donde esta el page "User". En el Postman puedes probar la API: verperfil.
      .then(response => {
        if (!response) toast.error("El usuario que has visitado no existe"); //si el response es diferente que null entonces que aparezca el toast.error
        setUser(response) //El setUser(response) si o si se ejecuta, no respeta el if porque no esta relacionado con el if mediante los corchetes "{}".
        //console.log(response)
      })
      .catch(() => {
        //console.log(response)
        toast.error("El usuario que has visitado no existe");
      });
  

  }, [params]) //Este params esta aqui porque, cuando el usuario en el http://localhost:3000/623a46feecd3acdc813f84e5(params) se realize un "cambio" asi por ejemplo http://localhost:3000/asdasdasd(params), se realize otra vez la ejecucion de la funcion "useEffect"
  //basicamente cuando se realize un cambio en params se vuelva a ejecutar el useEffect

  useEffect(() => {
    getUserTweetsApi(params.id, 1).then(response => {
      setTweets(response);
      //console.log(response)
    })
    .catch(() => {
      setTweets([]);
    });
  }, [params]);
  
  const moreData = () => {  //esta funcion agrega mas tweets solo cuando se hace click en el boton de "Obtener mas Tweets"
    const pageTemp = page + 1;
    setLoadingTweets(true); //cuando hace click en el boton de "Obtener mas Tweets" se ejecutara el setLoadingTweets(true) y se ejecutara el Spinner. Esto se queda en true cuando en el getUserTweetsApi ya no hay mas tweets del usuario (!response)
    //console.log("Buscando mas tweets...")
    
    //una vez se ejecute el spinner se ejecutara la siguiente funcion getUserTweetsApi para verficar si hay mas tweets del usuario
    getUserTweetsApi(params.id, pageTemp).then((response) => {
      if (!response) {   //si no hay tweets entonces se ejecutara el setLoadingTweets(0)
        console.log("No hay mas tweets")
        setLoadingTweets(0); //esto es para que no salga el boton de "Obtener mas Tweets"
        } else {  //si hay tweets se ejecutara las siguientes lineas
          console.log("todavia hay tweets (solo muestra 10 tweets por cada click en el boton de Obtener mas Tweets")
          setTweets([...tweets, ...response]); // En "...tweets" esta primeramente los 10 primeros tweets del usuario y en el "...response" estan los 10 siguientes tweets del usuario, lo cual lo esta adjuntando en el setTweets todos los tweets del usuario
          setPage(pageTemp); //esto es para que se vaya sumando el numero de pagina
          setLoadingTweets(false); //esto para que se vuelva a ejecutar el boton de "Obtener mas Tweets"
        }
      });
  };

  return (
    <BasicLayout className="user" setRefreshCheckLogin={setRefreshCheckLogin}>
      <div className="user__title">
        {user ? `${user.nombre} ${user.apellidos}`: "Este usuario no existe"} {/*Si user tiene datos(response) entonces `${user.nombre} ${user.apellidos}` y si no tiene datos(null) entonces : "Este usuario no existe"*/}
      </div>
      <BannerAvatar user={user} loggedUser={loggedUser} /> {/*Aqui le esta enviando el user(response) y el loggedUser(useAuth(el token del usuario logeado)) al BannerAvatar mediante los props*/}
      <InfoUser user={user} />
      <div className='user__tweets'>
        <h3>Tweets</h3>
          {(tweets ? (<ListTweets tweets={tweets} />):(<h5>"Todavia no hay Tweets de este Usuario"</h5>))}
          <Button onClick={moreData}>
            {!loadingTweets ? ( (loadingTweets !== 0) && "Obtener mas Tweets"):(<Spinner as="span" animation="grow" size="sm" role="status" arian-hidden="true" />)} 
            {/*si loadingTweets es false entonces le aparecera en el boton "Obtener mas Tweets", caso contrario si loadingTweets es true, igual a 0 entonces se ejecutara el Spinner y el botton no contendra nada "" pero desaparecera. Eso de que el botton desaparezca esta en el User.scss(linea 23)*/}
          </Button>
        </div>
    </BasicLayout>
  );

  {/*<h3>Tweets</h3>
        {tweets && <ListTweets tweets={tweets}/>  // si tweets nos es null ejecutara <ListTweets />
    </div>}
  </BasicLayout*/}
}


  

