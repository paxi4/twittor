import React, {useEffect, useState} from 'react';
import BasicLayout from '../../layout/BasicLayout';
import {Button, Spinner} from 'react-bootstrap';
import ListTweets from '../../components/ListTweets';
import {getTweetsFollowersApi} from '../../api/tweet';

import "./Home.scss";

export default function Home(props) {
  const {setRefreshCheckLogin} = props; // el setRefreshCheckLogin proviene del Routing.js
  const [tweets, setTweets] = useState(null);  // aqui tweets = null.
  const [page, setPage] = useState(1);
  const [loadingTweets, setLoadingTweets] = useState(false);
  
  useEffect(() => {
    getTweetsFollowersApi(page).then(response => {
      //En la pagina de "Inicio" primero esta entrando tweets como "null" porque asi esta por defecto en la linea 11
      if (!tweets && response ) {  //Si tweets es null y response tiene datos. Cuando haces Click en el boton "Inicio" primero se carga en esta parte los 20 tweets de los usuarios amigos
        // el response esta ahi porque si el usuario no sigue a nadie y hace click en el boton "Obtener mas tweets", le aparecera un spinner infinito porque no a encontrado mas tweets de los usuarios que sigue.
        //console.log(tweets) // Aqui entra como null por la linea 11
        setTweets(formatModel(response));  //Aqui se estan insertando en "tweets" los primeros 20 tweets de los usuarios amigos, pero en el formato de la funciom "formatModel"
        // Si no se pone el formatModel el componente "ListTweets" no podra leer los tweets porque no estan en el formato que el componente "ListTweets" esta esperando.
        //console.log(response); //Aqui esta botando los 20 tweets de los usuarios amigos.
      } else { //Si tweets tiene contenido. Osea si ya tiene los 20 tweets de los usuarios amigos y hace click en el boton "Obtener mas tweets".
        if (!response) { //Si en response no tiene contenido, significa que no a encontrado mas tweets de los usuarios que sigue. 
          setLoadingTweets(0); //Esto es para que no ya no se muestre el boton de "Obtener mas tweets"
        } else { //Si response tiene contenido, significa que si a encontrado mas tweets de los usuarios que sigue. 
          //console.log(response); //Aqui esta botando mas tweets de los 20 tweets usuarios amigos.
          const data = formatModel(response); //Aqui se estan insertando en "data" los nuevos tweets que se han encontrado, pero en el formato de la funciom "formatModel"
          // Si no se pone el formatModel el componente "ListTweets" no podra leer los tweets porque no estan en el formato que el componente "ListTweets" esta esperando.
          setTweets([...tweets, ...data]);  //en (...tweets) estan los 20 tweets de los usuarios seguidos y en (...data) estan los nuevos tweets que se han encontrado.
          setLoadingTweets(false); //Esto es para que se muestre el boton de "Obtener mas tweets"
        }
      }
    }).catch(() => {});
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const moreData = () => {
    //console.log("Cargando...")
    setLoadingTweets(true);  //Esto es para que se muestre el spinner.
    setPage(page + 1); //Aqui se esta aumentando la pagina para que se carguen mas tweets.
  }

  return (
    <BasicLayout className="home" setRefreshCheckLogin={setRefreshCheckLogin}> {/*Aqui basicamente el contenido de "BasicLayout" se mostrara en el Home || Si el BasicLayout envuelve un contenido entonces aparecera un children y si no aparecera || El className se pone para mandarlo al "basicLayout.js" de esa manera se identifica bien la pagina home*/}
      <div className='home__title'>
        <h2>Inicio</h2> {/*Este contenido ahora le pertene al "BasicLayout" como su "children", porque lo esta envolviendo el "BasicLayout"*/}
      </div>
      {tweets && <ListTweets tweets={tweets} />} {/*Si tweets tiene contenido entonces se mostrara el componente "ListTweets" y si no no se mostrara nada.*/}
      <Button onClick={moreData} className="load-more">
        {!loadingTweets ? ( //Si loadingTweets es false entonces se mostrara el texto "Obtener mas tweets" y si es true entonces se mostrara el spinner.
          loadingTweets !== 0 ? "Obtener mas tweets" : "No hay mas tweets" //Si loadingTweets es diferente de 0 entonces se mostrara el texto "Obtener mas tweets" y si es 0 entonces se mostrara nada "No hay mas tweets".
        ) : ( //Si loadingTweets es true entonces se mostrara el spinner.
          <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true"/>
        )}
      </Button>
    </BasicLayout>
  );
}

function formatModel(tweets){ //Esta funcion hace que los tweets que esta recibiendo de la api se adapten al formato que el componente "ListTweets" esta esperando.
  const tweetsTemp = [];
  tweets.forEach((tweet) => { // Aqui se esta recorriendo el array de tweets que esta recibiendo de la api y se esta insertando en el array "tweetsTemp" los tweets en el formato que el componente "ListTweets" esta esperando.
    tweetsTemp.push({
      _id: tweet._id,
      userId: tweet.userRelationId,
      mensaje: tweet.Tweet.mensaje,
      fecha: tweet.Tweet.fecha
    });
  });
  //console.log(tweetsTemp)
  return tweetsTemp;
}
