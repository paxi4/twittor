import React, {useState} from "react";
import { Container, Row, Col, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import LogoTwittor from "../../assets/png/logo.png";
import LogoWhiteTwittor from '../../assets/png/logo-white.png';
import { faSearch, faUsers, faComment } from '@fortawesome/free-solid-svg-icons';
import BasicModal from "../../components/Modal/BasicModal";
import SignUpForm from "../../components/SignUpForm";
import SignInForm from "../../components/SignInForm";


import "./SignlnSingUp.scss";

export default function SignlnSingUp(props) {
    const {setRefreshCheckLogin} = props;
    const [showModal, setShowModal]= useState(false); //aqui esta en false el showModal por default por el useState.
    const [contentModal, setContentModal] = useState(null); //El contentModal como default tiene un null por el useState.

    const openModal = content => { //esta funcion se activa solo cuando se hace click en "Registrate" y "Iniciar Sesion"
        setShowModal(true); //Cuando el usuario haga click en el boton "Registrate" o "Iniciar Sesion", automaticamente le aparecera el formulario
        setContentModal(content); //Aqui estas agregando al setContentModal (el "content") que vendria ser los datos de SignInFrom.js o SignUpForm.js
        //console.log("xd")
    };

    return (
    <>
        <Container className="signin-signup" fluid> 
            <Row>
                <LeftComponent />
                <RightComponent openModal={openModal} setShowModal={setShowModal} setRefreshCheckLogin={setRefreshCheckLogin} />
            </Row>
        </Container>
        <BasicModal show={showModal} setShow={setShowModal}> {/*Este Basic Modal es la ventana dentro del boton Registrate y Iniciar Sesion */}
            {contentModal} {/*el contentModal esta relacionado con el setContentModal || el "contentModal" vendria ser el children en el BasicModal.jss  */}
        </BasicModal>
    </>
  );
}

function LeftComponent(){
    return (
        <Col className="signin-signup__left" xs={6}>
            <img src={LogoTwittor} alt="Twittor" />
            <div>
                <h2>
                    <FontAwesomeIcon icon={faSearch} />
                     Sigue lo que te interesa
                </h2>
                <h2> 
                    <FontAwesomeIcon icon={faUsers} />
                    Enterate de que esta hablando la gente.
                </h2>
                <h2>
                    <FontAwesomeIcon icon={faComment} />
                    Unete a la conversacion.
                </h2>
            </div>    
        </Col>
    );
}

function RightComponent(props){
    const {openModal, setRefreshCheckLogin} = props //esta linea de codigo lo pone para usar el javascript de este archivo

    return (
        <Col className="signin-signup__right" xs={6}>
            <div>
                <img src={LogoWhiteTwittor} alt="Twittor" />
                <h2>Mira lo que esta pasando en el mundo en este momento</h2>
                <h3>unete a Twittor hoy mismo.</h3>
                <Button 
                variant="primary"
                onClick={()=>openModal(<SignUpForm />)} //Aqui no llama al setShowModal porque en la funcion openModal ya esta llamando al setShowModal
                >
                    Registrate
                </Button>
                <Button 
                variant="outline-primary"
                onClick={()=>openModal(<SignInForm setRefreshCheckLogin={setRefreshCheckLogin} />)} //Aqui no llama al setShowModal porque en la funcion openModal ya esta llamando al setShowModal
                >
                    Iniciar Sesion
                </Button>
            </div>
        </Col>
    );
}
