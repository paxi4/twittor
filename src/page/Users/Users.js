import { parse } from 'date-fns';
import { isEmpty } from 'lodash';
import queryString from 'query-string';
import React, {useState, useEffect} from 'react'
import { Spinner,ButtonGroup, Button } from 'react-bootstrap'
import { useLocation,useSearchParams } from 'react-router-dom';
import { useDebouncedCallback } from 'use-debounce';
import { getFollowersApi } from '../../api/follow';
import ListUsers from '../../components/ListUsers';
import BasicLayout from '../../layout/BasicLayout'

import "./Users.scss"

export default function Users(props) {

  const {setRefreshCheckLogin} = props; 
  const [users, setUsers]= useState(null); 
  const [btnLoading, setBtnLoading]= useState(false); 
  const location = useLocation();   // El useLocation extrae los datos solo de la URL: http://localhost:3000/(users?page=1&type=new&search=)<-- Esto usa el useLocation, usar el console.log(params).
  const params = useUsersQuery(location); // Aqui al location le extraen el dato(string) de la variable "search" que vendria ser esto: ?page=1&type=new&search= y lo covierten en un objeto: params2: { page: '1', type: 'new', search: '' }  en el console.log(params) veras a que me refiero.
  const [typeUser, setTypeUser] = useState(params.type || "follow"); //si params.type no tiene un valor asignado, entonces sera "follow" caso contrario sera el params.type
  const [history, setHistory] = useSearchParams();  //el useSearchParams permite que se pueda cambiar el valor de la URL: http://localhost:3000/users(?page=1&type=new&search=)<-- Esto usa el useSearchParams.
  //console.log(location); //RESULTADO: {pathname: '/users', search: '', hash: '', state: null, key: 'k9t52xcb'}
  //console.log(params); //RESULTADO: params: { page: '1', type: 'new', search: '' }
  //console.log(queryString.stringify(params2)); //RESULTADO: queryString.stringify(params2): 'page=1&search=&type=new'
  
  const [onSearch] = useDebouncedCallback( (value) => {
    //console.log(value)
    setUsers(null);
    setHistory(queryString.stringify({ ...params, page: 1,search: value}));  // El (...params) extrae todos los datos de params(page,type,search), en este caso solo mantendria al "type" porque no se esta utilizando, solo se esta utilizando al search y page.
    // Si todavia no queda claro con el (...params) prueba a quitarlo, haz click y busca un usuario de 2, usa el console.log(params) y veras el error.
    // Para salir de dudas del (...params) mira este video: https://www.youtube.com/watch?v=a_hye_eCULo
    // Por su parte, el método stringify nos permite transformar un objeto a un string.
  },200); // 200 es el tiempo que tarda en ejecutarse la funcion, ayuda al buscador a que no se ejecute cada vez que se escribe una letra.
  

  //Al comienzo este useEffect se ejecuta, pero si se hace un cambio en
  useEffect(() => {
    //console.log("HOLAAAA")
    // El getFollowersApi solo acepta los datos(page,type,search) de tipo "string", en este caso el metodo stringify convierte el objeto params en un string, por lo tanto siempre va a dar correcto para el getFollowersApi.
    // Por su parte, el método stringify nos permite transformar un objeto a un string.
    // si se quita el queryString.stringify no funcionara, porque al comienzo el params es un objeto por la funcion useUsersQuery, por lo cual no aceptara la funcion getFollowersApi el params porque no es string. Se recomiendo ver esta prueba con el console.log(params)
    getFollowersApi(queryString.stringify(params)) // el params si o si va a respetar el dato de la URL: users?page=1&type=new&search= || el quweyString.stringify(params2) lo que hace es convertir el objeto params2 en un string: page=1&search=&type=new
    .then(response => {
      //console.log("hola1")
      // eslint-disable-next-line eqeqeq
      if (params.page == '1') { // Aqui es (==) porque al comienzo en el page tiene 1 tipo numero gracias a la funcion (useUsersQuery) pero cuando haces click en el boton "siguiendo" el 1 numero se convierte en string
                                // (==) el doble igual le esta diciendo que sea igual pero que acepte 1 no importa si es de tipo numero o string.
        if(isEmpty(response)){
          setUsers([]);
        } else {
          //console.log("hola2")
          setUsers(response);
          //console.log(location)
        }
      } else {
        if (!response) {  //Si no hay mas usuarios que mostrar, entonces se desactiva el boton "Cargar mas"
          setBtnLoading(0); // 0 es para que no se muestre el boton de "Cargar mas usuarios", y se muestre el spinner "invisible"
        } else {
          setUsers([...users, ...response]);  // Aqui se concatena el array de usuarios que ya se habian cargado con los nuevos usuarios que se cargaron.
          setBtnLoading(false)  // false es para que se muestre el boton "Cargar mas usuarios"
        }
      }  
    })
    .catch(() => {
      setUsers([]);
    })
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location])  //Cuando el location(?page=1&search=&type=follow) cambie, se ejecutara el useEffect

  const onChangeType = (type) => {  //Esta funcion se ejecuta cuando se hace click en el boton "Siguiendo" o "Nuevos" y toma el valor de "type" que es el que se le pasa en el onClick
    
    setUsers(null); //Esto es para que cuando cambies de tipo de usuario, se muestre el spinner de "Buscando Usuarios"
    setBtnLoading(false); //Esto esta para cuando primero hagas clic en "Cargar mas usuarios" hasta que desaparezca y luego hagas click en "Nuevos", el boton no desaparezca y se reestablezca de nuevo.
    //la logica del if esta solo para que cuando se haga click en el boton "Siguiendo" o "Nuevos" se le asigne la clase "active" al boton que se le hizo click. En el Users.scss &__options en &.active se le asigna el color azul al boton que se le hizo click.
    //esta logica apoya a la logica de los botones "Siguiendo" y "Nuevos".
    if(type === "new"){
      setTypeUser("new");
    } else {
      setTypeUser("follow");
    }
    
    setHistory( //setHistory es una funcion que permite cambiar el valor de la URL: http://localhost:3000/users(?page=1&type=new&search=)<-- Esto usa el useSearchParams.
       queryString.stringify({ ...params, page: 1, type: type})  //Esto vendria ser en la URL: (?page=1&search=&type=new) cuando para por el stringify se convierte en un string: (page=1&search=&type=new)
       // importante dejar el ...params siempre en la primera posicion: si se pone el paramas de esta manera: { type: type , ...params} ESTA MAL.
       // El page:1 es totalmente necesario porque si tu haces click en "Nuevo" y despues haces click en "Cargar mas usuarios", luego haces click en "Siguiendo" buscara en la pagina 2 lo cual no tienes usuarios. Usar el console.log(params)
    );
  };

  const moreData = () => {
    setBtnLoading(true); // true es para que se muestre el spinner
    const newPage = parseInt(params.page) + 1; // Esto suma 1 a la pagina actual y se lo asigna a la variable newPage. solo el "params.page" es string pero con el parseInt el string se vuele int y con ello si se puede sumar
    setHistory(queryString.stringify({ ...params, page: newPage}));
  }

  return (
    <BasicLayout className="users" title="Usuarios" setRefreshCheckLogin={setRefreshCheckLogin}> {/*el setRefreshCheckLogin verifica que el token del usuario no este caducado*/}
      <div className="users__title">
        <h2>Usuarios</h2>
        <input type="text" placeholder='Busca un usuario...' onChange={(e) => onSearch(e.target.value)} />
      </div>

      <ButtonGroup className='users__options'>
        <Button className={typeUser === "follow" && "active"} onClick={() => onChangeType("follow")}>Siguiendo</Button> {/*si typeUser es igual a "follow" va a tener la clase "active" por lo cual en el boton Siguiendo en la parte baja le aparecera una barra azul pequena || En el Users.scss vas a ver como funciona el "active"*/}
        <Button className={typeUser === "new" && "active"} onClick={() => onChangeType("new")}>Nuevos</Button> {/*si typeUser es igual a "new" va a tener la clase "active" por lo cual en el boton Siguiendo en la parte baja le aparecera una barra azul pequena || En el Users.scss vas a ver como funciona el "active"*/}
      </ButtonGroup>

      {!users ? ( // Si users es null, entonces se muestra el spinner de "Buscando Usuarios"
        <div className="users__loading">
          <Spinner animation="border" variant="info" />
          Buscando usuarios
        </div>
      ):(
        <>
          <ListUsers users={users} /> {/* Si users no es null, entonces se muestra la lista de usuarios*/}
          <Button onClick={moreData} className="load-more">
            {!btnLoading ? (  // Si btnLoading es false o null y si btnLoading es diferente que 0, entonces se muestra el boton "Cargar mas usuarios"
              btnLoading !== 0 && "Cargar mas usuarios"
            ) : ( // Si btnLoading es true, entonces se muestra el spinner.
              <Spinner as="span" animation='grow' size='sm' role="status" ariahidden="true" />
            )}
          </Button>
        </>
      )} 
    </BasicLayout>
  );
}

function useUsersQuery(location) {
  // el const hace que cuando hagas click en el boton "Usuarios" por defecto se muestren los usuarios que estas siguiendo por el type="follow"
  const {page = 1, type="follow", search=""} = queryString.parse(location.search); // (location.search) nos ayuda a extraer el contenido solo de la variable "search" del "location"
  // El método parse nos ayuda a transformar un string a un objeto en JavaScript. // Entonces esta transformando el string: ?page=1&type=new&search= en un objeto: {page: "1", type: "new", search: ""} de la variable "search". 
  return {page, type, search};
};
