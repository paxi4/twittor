import { useContext } from "react"; //el useContext se usa para Usar un "Contexto"
import { AuthContext } from "../utils/contexts"; //Este AuthContext contiene el token del usuario

export default () => useContext(AuthContext); //Aqui al AuthContext le esta asignando de esta manera: useContext=AuthContext 