//import { Button, Alert } from "react-bootstrap";
import SignlnSingUp from "./page/SignlnSingUp"; //aqui cuando no se especifica un archivo export de manera default el index.js
import React, {useState, useEffect} from "react";
import {ToastContainer} from "react-toastify";
import {AuthContext} from "./utils/contexts";
import { isUserLogedApi } from "./api/auth";
import Routing from "./routes/Routing";

export default function App(){
  const [user, setUser] = useState(null); //aqui el useState(null) no tiene datos, por defecto le aparecera el <SignlnSingUp />
  const [loadUser, setLoadUser] = useState(false); //
  const [refreshCheckLogin, setRefreshCheckLogin]= useState(false); //esto es para refrescar la pagina

  useEffect(()=> { //
    setUser(isUserLogedApi());  //Aqui al "user" si todo va bien en la funcion isUserLogedApi, entonces el user tendra como dato un "TOKEN(veryessdasdasd)" ||este codigo verifica que si una vez que te logeas bien, entonces procesara tu token y te votara todos tus datos como tu: nombre, apellido, etc.
    setRefreshCheckLogin(false); //Cuando el usuario se logee bien refresca el componente
    setLoadUser(true); //sinceramente nose porque lo puso "F"
    //console.log(isUserLogedApi()); //esta consola verifica que si una vez que te logeas bien, entonces procesara tu token y te votara todos tus datos como tu: nombre, apellido, etc.
  }, [refreshCheckLogin]); //aqui le esta diciendo a la funcion useEffect que cuando el "refresCheckLogin=true" actualize toda la funcion "useEffect", basicamente que recorra otra vez las funcion "useEffect."
  
  if (!loadUser) return null; //si loaduser es false retornamos null para que se vea en blanco. 

  return (
    <AuthContext.Provider value={user}> {/*Aca el contexto se esta utilizando en el valor "user" || Ahora el AuthContext tiene como valor el setUser(isUserLogedApi()) del usuario*/}
      {/*Si usuario(user) tiene datos(TOKEN) entonces <h1>Estas Logeado</h1> y si no <SignlnSingUp /> */}
      {user ? (
        {/*Si usuario(user) tiene datos(TOKEN) y el setRefreshCheckLogin=true, entonces le direccionara en el Routing a "/" */}, 
        <Routing setRefreshCheckLogin={setRefreshCheckLogin} />
      ) : (
        {/*Si usuario(user) no tiene datos(null) y el setRefreshCheckLogin=false, entonces le direccionara al SignlnSingUp */}, 
        <SignlnSingUp setRefreshCheckLogin={setRefreshCheckLogin} />
      )} 
    <ToastContainer
      position="top-right"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
      theme="colored" //esto lo pongo por el demo https://fkhadra.github.io/react-toastify/introduction/
     />
    </AuthContext.Provider>  
  );
}