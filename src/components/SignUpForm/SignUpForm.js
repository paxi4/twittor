import React, {useState} from 'react';
import { Row, Col, Form, Button, Spinner } from "react-bootstrap";
import {values, size} from "lodash";
import {toast} from "react-toastify";
import {isEmailValid} from "../../utils/validations"
import {signUpApi} from "../../api/auth"

import "./SignUpForm.scss";

export default function SignUpForm(props) {
  const {setShowModal} = props;  //esta linea de codigo esta aqui porque esta llamando del SignlnSingUp.js al setShowModal
  const [formData, setFormData]=useState(initialFormValue()); //el formData tiene los atributos del "initialFormvalue()"
  const [signUpLoading, setSignUpLoading] = useState(false); //esto lo pone para controlar cuando se de click al boton Registrarse, si se registra bien sale la animacion del spinner
  
  const onSubmit = e =>{ //la "e" se pone para que no se refresce el login o sea la pagina 
      
      e.preventDefault();
      //setShowModal(false); // esto hace que desaparesca el formulario de "Registrate"
      //console.log(formData); //esto muestra los valores del "formData" 
      //console.log(size(formData)); // el size obtiene todos los parametros del formData como su: nombre, apellido, etc y eso da un total de 5

      let validCount = 0;
      values(formData).some(value =>{ //El "values" basicamente cuenta los valores del formulario, es como un for; El value son los "valores" rellenados en los inputs del formulario
          value && validCount++; //aqui esta contando del formulario los datos rellenados por 1,2,3,4 y etc
          return null;
      });

      if (validCount !== size(formData)){ //esta condicion dice "si el validCount es diferente a 5 que vedria ser el total del parametro del formData"
          toast.warning("Completa todos los campos del formulario")
      } else {
          if (!isEmailValid(formData.email)) { //si el email es invalido, es lo que hace esto "!isEmailValid"
            toast.warning("Email invalido");
          } else if (formData.password !== formData.repeatPassword){ //si el password y el repeatPassword son diferentes
              toast.warning("Las contrasenas tienen que ser iguales");
          } else if (size(formData.password)< 6){ //si el password es menor a 6
              toast.warning("La contrasena tiene que tener al menos 6 caracteres")
          } else {
              setSignUpLoading(true);
              signUpApi(formData)
                .then(response => {
                    if (response.code) { //si el response es un code, lo puedes ver en el auth.js linea 27
                        toast.warning(response.message); //entonces aparecera un menssage
                    } else {
                        toast.success("El registro has sido correcto");
                        setShowModal(false);
                        setFormData(initialFormValue());
                    }
                })
                .catch(() => {
                    toast.error("Error del servidor, intentelo mas tarde!");
                })
                .finally (()=>{ //esto se usa cuando acabe toda la peticion completamente
                    setSignUpLoading(false);
                });
          }
      }

      //console.log(validCount); // el "validCount" cuenta cuando rellena los inputs y apreta el boton, de acuerdo ha eso el validCount cuenta en tiempor real.
  };

  const onChange = e =>{ //esta tecnica solo funciona solo para formularios hechos con solo imputs
      //console.log(e.target.name); //Cuando en el input se agrega algo esto aumenta valores del input agregado, puedes verlo en la consola del google chrome
      setFormData({ ...formData, [e.target.name]: e.target.value }); // ..formData es para que salga los demas datos como el nombre, apellidos, etc., sin esto solo aprecera uno.
  };

  return (
      <div className='sign-up-form'>
          <h2>Crea tu cuenta</h2>
          <Form onSubmit={onSubmit} onChange={onChange}>
              <Form.Group className='form-group'> {/*Agregar el className al "Form.Group" para que en el scss lo reconozca*/}
                <Row>
                    <Col>
                        <Form.Control type='text' placeholder='Nombre' name="nombre" defaultValue={formData.nombre} /*value={formData.nombre} onChange={e => setFormData({ ...formData, nombre: e.target.value})} Usalo en caso de que no sea input*/ /> 
                    </Col>
                    <Col>
                        <Form.Control type='text' placeholder='Apellidos' name='apellidos' defaultValue={formData.apellidos} />
                    </Col>
                </Row>
              </Form.Group>
              <br></br>
              <Form.Group className='form-group'>
                  <Form.Control type='email' placeholder='Correo Electronico' name='email' defaultValue={formData.email} />
              </Form.Group>
              <br></br>
              <Form.Group className='form-group'>
                  <Row>
                      <Col>
                        <Form.Control type="password" placeholder='Contrasena' name='password' defaultValue={formData.password}/>
                      </Col>
                      <Col>
                        <Form.Control type="password" placeholder='Repetir contrasena' name='repeatPassword' 
                        defaultValue={formData.repeatPassword}/>
                      </Col>
                  </Row>
              </Form.Group >
              <br></br>
              <Button variant="primary" type="submit">
                  {!signUpLoading ? "Registrarse" : <Spinner animation='border' />}{/*si es signUpLoading=false entonces le aparecera Registrarse" y si no(true) le aparecera el Spinner*/}
              </Button>
          </Form>
      </div>
  );
}

function initialFormValue() {
    return {
        nombre: "", //esto es el value
        apellidos: "",
        email: "",
        password: "",
        repeatPassword: ""
    };
}
