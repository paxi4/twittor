import React, {useState} from 'react'
import { Modal, Form, Button } from 'react-bootstrap';
import classNames from 'classnames';
import {Close} from "../../../utils/icons";
import { addTweetApi } from '../../../api/tweet';
import { toast } from 'react-toastify';

import "./TweetModal.scss";

export default function TweetModal(props) {
  const {show, setShow} = props;
  const [message, setMessage] = useState("");
  const maxLength = 280;

  const onSubmit = e => {
    e.preventDefault();
    if(message.length > 0 && message.length <= maxLength){
      addTweetApi(message).then(response => {
        if (response?.code >= 200 && response?.code < 300) {
          toast.success(response.message);
          setShow(false);  //this is the function that closes the TweetModal
          window.location.reload(); //this is the function that reloads the page
        }
      })
      .catch(() => {
        toast.error("Error del servidor, intentelo más tarde.");
      });
  };
  } 
  return (
    <Modal className='tweet-modal' show={show} onHide={() => setShow(false)} centered size='lg'> {/* Aqui el show=showModal(false) por lo tanto le esta indicando que el modal debe estar ocultado*/}
        <Modal.Header>
            <Modal.Title>
                <Close onClick={() => setShow(false)} /> {/*Este el boton de X para cerrar el Modal */}
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form onSubmit={onSubmit}>
                <Form.Control as="textarea" rows="6" name="message" placeholder="¿Qué está pasando?" onChange={(e) => setMessage(e.target.value)} />  {/*El rows es el espacio que el usuario tiene para escribir, mientras mas menos rows halla como por ejemplo "rows=1"*/}
                <span className={classNames("count", {error: message.length > maxLength})}>{message.length}</span>
                <Button type="submit" disabled={message.length > maxLength || message.length < 1} >Twittoar</Button>
            </Form>
        </Modal.Body>   
    </Modal>
  );
}
