import React from "react";
import {Modal} from "react-bootstrap"
import LogoWhiteTwittor from "../../../assets/png/logo-white.png"

import "./Basicmodal.scss";

export default function BasicModal(props){
    const {show, setShow, children} = props;

    return (
        <Modal //El modal sabe que se debe mostrar cuando es true y cuando es false se debe ocultar, esta por default
            className="basic-modal" 
            show={show} //Esto recibe del SignInSingUp.js, que el "show" sea por defecto false.
            onHide={()=> setShow(false)} //Cuando esta en false, al hacer click fuera del modal se va tornar false y se va a cerrar el modal.
            centered size="lg"
        >
            <Modal.Header>
                <Modal.Title>
                    <img src={LogoWhiteTwittor} alt="Twittor" />
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>{children}</Modal.Body> {/*El children es basicamente el contenido como un h2 o cualquiera */}
        </Modal>
    );
}