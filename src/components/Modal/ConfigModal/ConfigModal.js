import React from 'react'
import { Modal } from 'react-bootstrap' //fijate en esto DON GIL, AQUI FALLAS
import { Close } from '../../../utils/icons'

import "./ConfigModal.scss"

export default function ConfigModal(props) {
  const {show, setShow, title, children} = props; //El show, setShow y title va hacia el BannerAvatar || esta en color verde
  return (
    <Modal //El modal sabe que se debe mostrar cuando es true y cuando es false se debe ocultar, esta por default.
        className="config-modal"
        show={show} //Esto recibe del BannerAvatar.js, que el "show" sea por defecto false.
        onHide={()=> setShow(false)} //Cuando esta en false, al hacer click fuera del modal se va tornar false y se va a cerrar el modal 
        centered
        size="lg"
    >
        <Modal.Header>
            <Modal.Title>
                <Close onClick={()=> setShow(false)} /> {/*Cuando se haga click en el "icono" Close se va a cerrar el modal */}
                <h2>{title}</h2>
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>{children}</Modal.Body>    
    </Modal>
    );  
}
