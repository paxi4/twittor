import React, {useState} from 'react';
import { Button } from 'react-bootstrap'; //si solo pones "bootstrap" te va a salir error si quieres poner el Button
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faUser, faUsers, faPowerOff } from '@fortawesome/free-solid-svg-icons';
import TweetModal from '../Modal/TweetModal/TweetModal';
import LogoWhite from "../../assets/png/logo-white.png";
import {logoutApi} from "../../api/auth";
import useAuth from '../../hooks/useAuth';

import "./LeftMenu.scss";

export default function LeftMenu(props) {
  const {setRefreshCheckLogin} = props; //este setRefreshCheckLogin proviene del BasicLayout.js(linea 16)
  const [showModal, setShowModal] = useState(false);
  const user = useAuth(); //Aqui contiene toda la informacion del "usuario logeado", para salir de duda la clave esta en App.js. || por cierto el nombre useAuth proviene de useAuth.js
  //console.log(user)
  //console.log(props); //aqui me vota todos los datos de usuario como: _id, nombre, apellido, etc.

  const logout = () => {
    logoutApi(); //esto remueve el token, osea deslogea al usuario del twittor
    //window.location.reload(); este codigo recarga toda la pagina y no esta siguiendo las buenas practicas de "react"
    setRefreshCheckLogin(true); //Si esto es true entonces refrescara en el App.js la funcion useEffect([refreshCheckLogin]), porque el refreshCheckLogin proviene originariamente del App.js, y si se cambia a true el refreshCheckLogin se pasa otra vez la funcion con los parametros en este caso solo tomaria el LogoutApi y el setRefreshCheckLogin(true)
  }

  return (
    <div className='left-menu'>
        <img className='logo' src={LogoWhite} alt="Twittor" />

        <Link to="/"><FontAwesomeIcon icon={faHome} /> Inicio</Link> {/*El "href" recarga toda la pagina mientras que el "Link" solo los componentes */}
        <Link to="/users"><FontAwesomeIcon icon={faUsers} /> Usuarios</Link>
        <Link to={`/${user?._id}`}><FontAwesomeIcon icon={faUser} /> Perfil</Link> {/*El `/${user?._id}` nos dice si en el user no esta el _id entonces lo va a obviar y no lo va a ejecutar || Esto es como una validacion*/}
        <Link to="" onClick={logout}><FontAwesomeIcon  icon={faPowerOff} /> Cerrar sesion</Link>

        <Button onClick={() => setShowModal(true)}>Twittoar</Button>

        <TweetModal show={showModal} setShow={setShowModal} /> {/*Este modal "TweetModal" aparecera solo si por default el showModal es true pero en este caso esta false*/}
    </div>
  );
}
