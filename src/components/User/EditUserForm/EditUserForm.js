import React, {useCallback, useState, useReducer} from 'react';
import { Form, Button, Row, Col, Spinner } from 'react-bootstrap';
import DatePicker from "react-datepicker"; //Esta libreria nos ayuda a anadir un input a la fecha, para mas informacion: https://reactdatepicker.com
import es from "date-fns/locale/es"; //Esta libreria nos ayuda a cambiar la fecha a otro idioma
import { useDropzone } from 'react-dropzone'; //Esta libreria es como si fuera un input file
import { API_HOST } from '../../../utils/constant';
import { Camera } from '../../../utils/icons';
import { uploadBannerApi, updateInfoApi, uploadAvatarApi } from '../../../api/user';
import { toast } from 'react-toastify';

import "./EditUserForm.scss";
import { faWindowRestore } from '@fortawesome/free-solid-svg-icons';

export default function EditUserForm(props) {
  
  const {user, setShowModal} = props;
  const [formData, setFormData] = useState(initialValue(user));
  const [bannerUrl, setBannerUrl] = useState(user?.banner ? `${API_HOST}/obtenerBanner?id=${user.id}`: null);
  const [bannerFile, setBannerFile] = useState(null);
  const [avatarUrl, setAvatarUrl] = useState(user?.avatar ? `${API_HOST}/obtenerAvatar?id=${user.id}`: null);
  const [avatarFile, setAvatarFile] = useState(null);
  const [loading, setLoading] = useState(false);

  //const [reducerValue, forceUpdate] = useReducer(x => x + 1, 0);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onDropAvatar = useCallback(acceptedFile => { //El acceptedFile es la nueva imagen que se piensa subir, no es el banner de por defecto desde el back-end
    const file = acceptedFile[0]; //esta en posicion 0 porque en ahi esta la imagen, lo puedes ver en console.log
    setAvatarUrl(URL.createObjectURL(file))
    setAvatarFile(file);
    //console.log(file.path) //con esto puedes acceder a mas informacion dentro del file, como por ejemplo al lastModified
    //console.log(acceptedFile);
  });

  const {getRootProps: getRootAvatarProps, getInputProps: getInputAvatarProps} = useDropzone // Aca al "getRootProps: getRootBannerProps", el getRootProps esta teniendo como alias getRootBannerProps 
  ({ accept: "image/jpeg, image/png", noKeyboard: true, multiple:false, onDrop: onDropAvatar}); // El accept: "image/jpeg, image/png", solo permite subir imagenes de tipo jpeg y png || El multiple: false, solo permite subir solo una imagen a la vez || El onDrop permite que arrastre la imagen hasta el input

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onDropBanner = useCallback(acceptedFile => { //El acceptedFile es la nueva imagen que se piensa subir, no es el banner de por defecto desde el back-end
    const file = acceptedFile[0]; //esta en posicion 0 porque en ahi esta la imagen, lo puedes ver en console.log
    setBannerUrl(URL.createObjectURL(file))
    setBannerFile(file);
    //console.log(file.path) //con esto puedes acceder a mas informacion dentro del file, como por ejemplo al lastModified
    //console.log(acceptedFile);
  });

  const {getRootProps: getRootBannerProps, getInputProps: getInputBannerProps} = useDropzone // Aca al "getRootProps: getRootBannerProps", el getRootProps esta teniendo como alias getRootBannerProps 
  ({ accept: "image/jpeg, image/png", noKeyboard: true, multiple:false, onDrop: onDropBanner}); // El accept: "image/jpeg, image/png", solo permite subir imagenes de tipo jpeg y png || El multiple: false, solo permite subir solo una imagen a la vez || El onDrop permite que arrastre la imagen hasta el input

  const onChange = e => { //Esto solo funciona en los Form.Control, no funciona en DatePicker porque el date no tiene e.target.name y e.target.value ||
    setFormData({ ...formData, [e.target.name]: e.target.value})
    //console.log("Editando en tiempo Real ....")
  };
  
  const onSubmit = async (e) => {  //El async es para que espere a que se suba la imagen primero y luego ejecute las demas funciones
    e.preventDefault();
    setLoading(true);  //Cuando se envie el formulario, se pondra en true el loading.
    //console.log(props)
    if (bannerFile) { //si bannerFile existe=true entonces, ejecutara el uploadBannerApi
        // await lo que hace es que esperara a que se ejecute la funcion uploadBannerApi, y cuando se ejecute, entonces ejecutara la siguiente funcion
        await uploadBannerApi(bannerFile).catch(() => { //si uploadBannerApi recibe datos entonces ejecutara la funcion, y si no saldra error el toast
            toast.error("Error al subir el nuevo banner")
        });
    }

    if (avatarFile) { //si bannerFile existe=true entonces, ejecutara el uploadAvatarApi
        // await lo que hace es que esperara a que se ejecute la funcion uploadAvatarApi, y cuando se ejecute, entonces ejecutara la siguiente funcion
        await uploadAvatarApi(avatarFile).catch(() => { //si uploadBannerApi recibe datos entonces ejecutara la funcion, y si no saldra error el toast
            toast.error("Error al subir el nuevo avatar")
        });
    } else {    
        
    }
    // await lo que hace es que esperara a que se ejecute la funcion updateInfoApi, y cuando se ejecute, entonces ejecutara la siguiente funcion
    await updateInfoApi(formData).then(()=> {
        setShowModal(false);
    })
    .catch(()=> {
        toast.error("Error al actualizar los datos")
    });
    setLoading(false);  //Esto es para que se quite el loading osea el Spinner
    window.location.reload();   //Esto es para que recargue la pagina y se vea el nuevo banner y avatar
  };

  return (
    <div className='edit-user-form'>
        <div className="banner" style={{backgroundImage: `url('${bannerUrl}')`}} {...getRootBannerProps()}> {/*Para poder usar el input file necesitas declarar esto en el div - "getRootBannerProps"*/}
            <input {...getInputBannerProps()} /> {/*Aca al input lo esta declarando como un tipo file con el "getInputBannerProps"*/}
            <Camera />
        </div>
        <div className="avatar" style={{backgroundImage: `url('${avatarUrl}')`}} {...getRootAvatarProps()}> {/*Para poder usar el input file necesitas declarar esto en el div - "getRootBannerProps"*/}
            <input {...getInputAvatarProps()} /> {/*Aca al input lo esta declarando como un tipo file con el "getInputBannerProps"*/}
            <Camera />
        </div>
        <br></br>
        <br></br>
        <Form onSubmit={onSubmit}>
            <Form.Group>
                <Row>
                    <Col>
                        <Form.Control type="text" placeholder="Nombre" name="nombre" defaultValue={formData.nombre} onChange={onChange} />
                    </Col>
                    <Col>
                        <Form.Control type="text" placeholder="Apellidos" name="apellidos" defaultValue={formData.apellidos} onChange={onChange} />
                    </Col>
                </Row>
                <br></br>
                <Form.Group>
                    <Form.Control as="textarea" row="3" placeholder="Agregar tu biografia" type="text" name="biografia" defaultValue={formData.biografia} onChange={onChange} />
                </Form.Group>
                <br></br>
                <Form.Group>
                    <Form.Control type="text" placeholder="Sitio Web" name="sitioWeb" defaultValue={formData.sitioweb} onChange={onChange} />
                </Form.Group>
                <br></br>
                <Form.Group>
                    <DatePicker placeholder="Fecha de nacimiento" locale={es} selected={new Date(formData.fechaNacimiento)} onChange={date => setFormData ({ ...formData, fechaNacimiento: date})}  /> {/*Para comprobar tu codigo usa esto en el onChange: "e => console.log(e)" */}
                </Form.Group>
                <br></br>
                <Button className="btn-submit" variant="primary" type="submit">
                    {loading && <Spinner animation="border" size='sm' />} Actualizar  {/*La animacion del Spinner solo aparecera cuando el loading este en true, en caso de que el loading este en false la animacion del Spinner no aparecera*/}
                </Button>
            </Form.Group>
        </Form>
    </div>
  );
}

function initialValue(user){ //los datos del user deben coincidir con el de la base de datos
    return {
        nombre: user.nombre || "", //si user.nombre existe pones los datos, "pero si no(||)" me pones nada("").,
        apellidos: user.apellidos || "", 
        biografia: user.biografia || "",
        ubicacion: user.ubicacion || "",
        sitioweb: user.sitioweb || "",
        fechaNacimiento: user.fechaNacimiento || ""
    };
}