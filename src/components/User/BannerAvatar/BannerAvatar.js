import React, {useState, useEffect} from 'react'
import { Button } from 'react-bootstrap';
import ConfigModal from '../../Modal/ConfigModal';
import EditUserForm from '../EditUserForm';
import AvatarNoFound from '../../../assets/png/avatar-no-found.png'
import { API_HOST } from '../../../utils/constant';
import { checkFollowApi, followUserApi, unFollowUserApi } from '../../../api/follow';

import './BannerAvatar.scss';
import { initial } from 'lodash';

export default function BannerAvatar(props) {
  
  const {user, loggedUser} = props; //este props recibe el user(response) donde esta el User.js
  const [showModal, setShowModal] = useState(false); //Aqui por defecto el showModal tiene un false
  const [following, setFollowing] = useState(null); //Aqui por defecto el following tiene un null
  const [reloadFollow, setReloadFollow] = useState(false);
  const bannerUrl = user?.banner ? `${API_HOST}/obtenerBanner?id=${user.id}`: null; // si el user?.banner existe o tiene datos entonces ejecutara ${API_HOST}/obtenerBanner?id=${user.id} y si no o tiene datos en el banner entonces ejecutara null 
  const avatarUrl = user?.avatar ? `${API_HOST}/obtenerAvatar?id=${user.id}`: AvatarNoFound; // si el user?.banner existe o tiene datos entonces ejecutara ${API_HOST}/obtenerBanner?id=${user.id} y si no o tiene datos en el banner entonces ejecutara null 
  
  // this usseEffect is for check if the user is following or not
  useEffect(() => {
    if (user) {  // Se puso este if porque en el checkFollowApi entraba como following(null) primeramente botaba un false y luego buscaba el id del usuario buscado
      checkFollowApi(user?.id).then(response => { //the response is the status: true or false that returns the checkFollowApi(consultaRelacion)
        if (response?.status) {   //Si el usuario logeado le sigue al usuario buscado entonces el setFollowing(true) y si no le sigue entonces el setFollowing(false)
          setFollowing(true);
          //console.log(response); if at the console.log(response) you see the status: true, then the user is following
        } else {
          setFollowing(false);
          //console.log(response); if at the console.log(response) you see the status: false, then the user is not following
        }
      });
    }
    setReloadFollow(false);
  },[user, reloadFollow]);

  const onFollow = () => { 
    followUserApi(user.id).then(() => { //followUserApi(altaRelacion) is the function that follow the user
      setReloadFollow(true);
      //console.log("ALL OK");
    });
  };

  const onUnFollow = () => {
    unFollowUserApi(user.id).then(() => {
      setReloadFollow(true);
    });
  };

  return (
    <div className='banner-avatar' style={{backgroundImage: `url('${bannerUrl}')`}}>
        <div className='avatar' style={{backgroundImage: `url('${avatarUrl}')`}} />
        {user && ( {/*Si user existe entonces ejecutara el div de abajo*/},
          <div className='options'>
            {loggedUser._id === user.id && ( /*Si loggedUser._id(id del usuario logeado) === user.id(id del usuario buscado), entonces le aparecera el boton Editar Perfil*/
              <Button onClick={()=> setShowModal(true)}>Editar Perfil</Button> /* */
            )}
            
            {loggedUser._id !== user.id && following !== null && /*Si loggedUser._id(id del usuario logeado) es diferente a user.id(id del usuario buscado) y following(estado de relacion del usuario con otro) sea diferente a null ,entonces le aparecera el boton de "dejar de seguir" si es true(following(tiene relacion)) caso contrario le aparecera el boton de "seguir" si es false(following(no tiene relacion)) */
              (following ? (<Button onClick={onUnFollow} className="unfollow"><span>Siguiendo</span></Button>):(<Button onClick={onFollow}>Seguir</Button>) 
            )} {}
          </div>
        )}

      <ConfigModal show={showModal} setShow={setShowModal} title="Editar Perfil"> {/*El show, setShow y title va hacia el ConfigModal.js*/}
        <EditUserForm user={user} setShowModal={setShowModal}/>
      </ConfigModal>
    </div>
  )
}