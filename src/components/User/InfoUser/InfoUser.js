import React from 'react'
import moment from 'moment';
import localization from 'moment/locale/es'; //esto basicamente es en la version espanol
import { Location, Link, DateBirth } from '../../../utils/icons';
import "./InfoUser.scss"

export default function InfoUser(props) {
  const {user} = props;

  //console.log(user)

  return (
    <div className='info-user'>
        <h2 className="info-user">
            {user?.nombre} {user?.apellidos} {/*esto no tiene condicional porque por defecto el nombre y apellido si o si debe estar*/}
        </h2>
        <p className='email'>{user?.email}</p> {/*esto no tiene condicional porque por defecto el email si o si debe estar*/}
        {user?.biografia && <div className='description'>{user.biografia}</div>} {/*Si user?.biografia tiene datos mostrara el div descripction y si no tiene no mostrara nada */}

        <div className='more-info'>
            {user?.ubicacion && ( {/*Si user?.ubicacion tiene datos mostrara el <p> y si no tiene no mostrara nada */},
                <p> {/*La p es un parrafo */}
                    <Location />
                    {user.ubicacion}
                </p>
            )}
            {user?.sitioweb && (
                <a /*La a es para usar con un link de una pagina web*/
                    href={user.sitioweb}
                    alt={user.sitioweb}
                    target="_blank" /*el target y el rel lo ponemos para que cuando el usuario haga click, cree una pagina nueva y abra la pagina */
                    rel="noopener noreferrer"
                >
                    <Link /> {user.sitioweb}
                </a>
            )}
            {user?.fechaNacimiento && (
                <p>
                    <DateBirth />
                    {moment(user.fechaNacimiento).locale("es", localization).format("LL")} {/*El locale esta ahi para que la fecha salga en el idioma espanol(es) ||El format lo puedes ver en esta pagina: https://momentjs.com*/}
                </p>
            )}
        </div>
    </div>
  )
}
