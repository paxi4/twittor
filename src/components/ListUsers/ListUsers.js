import { map,isEmpty } from 'lodash';
import React from 'react'
import User from "./User"

import "./ListUsers.scss"

export default function ListUsers(props) {
  const {users} = props;
  
  if (isEmpty(users)){ //isEmpty es una funcion de lodash que verifica si el objeto esta vacio, en este caso si el objeto users esta vacio
    return <h2>No hay resultados</h2>
  }

  return (
    <ul className='list-users'>
      {map(users, (user) => (
        <User key={user.id} user={user} />
      ))}
    </ul>
  )
}
