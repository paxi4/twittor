import React, { useState,useEffect } from 'react';
import {Image, Stack} from 'react-bootstrap';
import {API_HOST} from '../../utils/constant';
import { Link } from 'react-router-dom';
import { getUserApi } from '../../api/user';
import AvatarNotFound from '../../assets/png/avatar-no-found.png';

export default function User(props) {
  const {user} = props;
  const [userInfo, setIserInfo] = useState(null);
  
  useEffect(() => {
    getUserApi(user.id).then(response => {
        //console.log(response)
        setIserInfo(response);
    });
    }, [user]);

  return (
    <Stack direction='horizontal' gap={3} as={Link} to={`/${user.id}`} className="list-users__user">
        <Image width={70} height={70} roundedCircle className="mr-3" src={userInfo?.avatar ? `${API_HOST}/obtenerAvatar?id=${user.id}` : AvatarNotFound} alt={`${user.nombre} ${user.apellidos}`} />
        <div className='Body'>
            <h5>{user.nombre} {user.apellidos}</h5>
            <p>{userInfo?.biografia}</p>
        </div>
    </Stack>
  )
}
