import React, {useState, useEffect} from 'react';
import { Image } from 'react-bootstrap';
import { map } from 'lodash';
import moment from 'moment';
import AvatarNotFound from '../../assets/png/avatar-no-found.png';
import { API_HOST } from '../../utils/constant.js';
import { getUserApi } from '../../api/user';
import { replaceURLWithHTMLLinks } from '../../utils/function.js';

import "./ListTweets.scss"

export default function ListTweets(props) {
  const {tweets} = props;

  return (
    <div className="list-tweets">
        {map(tweets, (tweet, index) => (   //Aqui esta leendo el array de tweets y los tweets=tweet, El index es el numero del tweet al que pertenece. Por ejemplo: tweet.mensaje: "Hola mundo", index: 0
            <Tweet key={index} tweet={tweet} />
        ))}
    </div>
  );
}

function Tweet(props){
  const {tweet} = props;
  const [userInfo, setUserInfo] = useState(null);
  const [avatarUrl, setAvatarUrl] = useState(null);
  //console.log(props); //aparece el tweet y el index
  //console.log(userInfo); //aparece el usuario que ha escrito el tweet
  //console.log(avatarUrl); //aparece la url del avatar del usuario que ha escrito el tweet

  useEffect(() => {
    getUserApi(tweet.userId).then(response => {  //El tweet."userId" en el postman(LeoTweets) cuando lo corras vas a ver que aparece el userId.
        //console.log(tweet) en tweet esta el tweet.mensaje
        setUserInfo(response)
        //console.log(userInfo) en UserInfo estan los datos(avatar,nombre,apellidos) del usuario "tweet.userId"
        setAvatarUrl(response?.avatar ? `${API_HOST}/obtenerAvatar?id=${response.id}` : AvatarNotFound);
    });
    }, [tweet])

    return ( 
        <div className="tweet">
            <Image className="avatar" src={avatarUrl} roundedCircle />
            <div>
                <div className="name">
                    {userInfo?.nombre} {userInfo?.apellidos}
                    <span>{moment(tweet.fecha).calendar()}</span>
                </div>
                <div dangerouslySetInnerHTML={{ __html: replaceURLWithHTMLLinks(tweet.mensaje)}} /> {/*este codigo hace que un link: (https://www.google.com) sea clickeable cuando el usuario haga un tweet*/}
            </div>
        </div>
    )
}

