import { API_HOST, TOKEN } from "../utils/constant";
import jwtDecode from "jwt-decode"; //esto sirve para decodificar el token

export function signUpApi(user){ //aqui el "user" es usado por el formData lo que vendria ser user=formData
    
    const url = `${API_HOST}/registro`;
    const userTemp = { //Este "userTemp" obtiene toda la informacion de formData del SignUpForm.js
        ...user, //esto "..." es como un array || aqui esta obteniendo todos los datos del formData, sin el "...user" no podria registrar al nuevo usuario
        //----------- El email y fecha vendrian ser como condicion -----------------------------------
        email: user.email.toLowerCase(), //esto hace que el email siempre se mande en minuscula, sin importar si el usuario lo halla puesto en mayuscula
        fechaNacimiento: new Date()
    };
    delete userTemp.repeatPassword; //aqui esta eliminando el atributo repeatPassword porque en el backend no esta el repeatPassword

    const params = {
        method: "POST", //gracias a esto el params le esta diciendo al fetch que el metodo es tipo POST
        headers: { 
            "Content-Type":"application/json" //esto de aqui es como el postman, te fijas en el headers del postman
        },
        body: JSON.stringify(userTemp) //Aqui esta obteniendo del userTemp el User(nombre, apellido, etc)
    };

    return fetch(url, params) //el fetch se usa para llamar una Request a una "API" es decir llamar a un servicio en la nube. El fetch por default obtiene un "GET"
        .then(response => { // el response en aqui vendria ser un formato request o cualquier otro pero o json
            if(response.status >= 200 && response.status < 300){ //si todo salio bien el response debe dar un status mayor o igual a 200
                return response.json(); //aqui el response retorna a formato json
            }
            return { code: 404, message: "Email no disponible"};
        })
        .then(result => {
            return result; //el result es cuando todo resulta bien
        })
        .catch(err => {
            return err; //el catch es en caso de error desconocido fuera de los errores que se puso en el back-end
        });
}

export function signInApi(user) {
    const url = `${API_HOST}/login`

    const data = {
        ...user,
        email: user.email.toLowerCase()
    };

    const params = {
        method: "POST",
        headers: {
            "Content-Type":"application/json"
        },
        body: JSON.stringify(data)
    };

    return fetch(url, params)
        .then(response => {
            if (response.status >= 200 && response.status < 300) { //si response.status es mayor o igual a 200 y response.status es mayor a 300 || dentro del 200 a 300 son status bueno en el POSTMAN osea en el back-end
                return response.json();
            }
            return {message : "Usuario o contrasena incorrectos"};
        })
        .then(result => {
            return result;
        })
        .catch(err => {
            return err;
        })
}

export function setTokenApi(token) { //esta funcion recive el token en el localStorage || esto setea
    localStorage.setItem(TOKEN, token); // en la consola lo puedes ver asi: "token(TOKEN) | eysa23asd2323casdas(token)" || Aqui el contenido del "token" lo esta asignando al TOKEN
}

export function getTokenApi(){ //esta funcion va a obtener el token del localStorage
    return localStorage.getItem(TOKEN);
}
//una Funcion solo tiene que hacer una cosa
export function logoutApi(){
    localStorage.removeItem(TOKEN);
}

export function isUserLogedApi(){ //esta funcion comprueba si el usuario esta logeado
    const token = getTokenApi();

    if (!token){ //si el token no existe osea es null, significa que no esta logeado || Si el token es direfente "que un token que tiene datos", por defecto lo toma asi el "if" 
        logoutApi(); 
        return null; //Esta condicion ya esta retornando 
    }

    if (isExpired(token)){ //si el token es true, osea que el token a caducado || la condicion "if" al "token" por defecto ya sabe que es true, porque no se comienza con el false; asi esta por defecto en javascript
        logoutApi(); //deslogea al usuario
    } else {
        return jwtDecode(token); //si el token es false, entonces no a caducado el token || devolvera el token descodificado con todos sus datos del usuario como su:nombre, apellido, etc || el "if" cuando el primero es true entonces por defecto el "else" es false esto esta por defecto en javascript
    }
}

function isExpired(token){ //devolvera true si el token a caducado y false en caso de que no a caducado
    const {exp} = jwtDecode(token); //aqui lo esta decodificando el token pero especificamente el "exp" que contiene la duracion del token
    const expire = exp * 1000; //aqui lo esta transformando en milisegundos osea asi: "202205061618"
    const timeout = expire - Date.now(); //El "expire" vendria ser la fecha,hora y minuto limite de expiracion del token y el Date.now() vendria ser la fecha actual con la hora y los minutos. || timeout ="202205061618"(expire) - "202205061518"(date.now)
    if (timeout < 0) { //si timeout es menor a 0 entonces el token a caducado || aqui el numero es negativo (-)
        return true;
    }
    return false; //si timeout es mayor a 0 entonces el token todavia no a caducado || aqui el numero es positivo (+)
}