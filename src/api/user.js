import { result } from "lodash";
import { API_HOST } from "../utils/constant";
import { getTokenApi } from "./auth";

export function getUserApi(id){
    const url = `${API_HOST}/verperfil?id=${id}`; //Este id lo va a recibir del params.id en el User.js

    const params = { 
       //method: "GET", //Si no se pasa una metodo este ya lo interpreta como "Get"
        headers: {
            "Content-Type":"application/json",
            Authorization: `Bearer ${getTokenApi()}` //El getTokenApi obtiene el token del usuario || para usar la funcion getUserApi necesitar tener un token por eso se pone la Authorization
        }
        
    };

    return fetch(url, params)
        .then(response => {
            // eslint-disable-next-line no-throw-literal
            if (response.status >= 400) throw null; //si el el response.status es mayor o igual a 400 entonces que salte directo al catch que seria el error
            return response.json();
        })
        .then(result => {
            return result;
        })
        .catch(err => {
            return err;
        });
}

export function uploadBannerApi(file) {
    const url = `${API_HOST}/subirBanner`;

    const formData = new FormData(); //El FormData acepta archivos de tipo file y strings || en el postman lo puedes ver como FormData
    formData.append("banner", file); //esta parte es como en el postman "SuboBanner" en body key:banner value:imagen.jpg
    //formData.append("banner", file); //Puedes seguir agregando mas imagenes descomentando esta linea
    console.log(formData);

    const params = {
        method: "POST",
        headers: {
            Authorization: `Bearer ${getTokenApi()}`
        },
        body: formData
    };

    return fetch(url, params)
        .then(response => { 
            return response.js //Aqui nos llega el header
        })
        .then(result => {
            return result; //Aqui nos llega el resultado decodificado
        })
        .catch(err => {
            return err;
        });
}

export function uploadAvatarApi(file) {
    const url = `${API_HOST}/subirAvatar`;

    const formData = new FormData(); //El FormData acepta archivos de tipo file y strings || en el postman lo puedes ver como FormData
    formData.append("avatar", file); //esta parte es como en el postman "SuboBanner" en body key:banner value:imagen.jpg
    //formData.append("banner", file); //Puedes seguir agregando mas imagenes descomentando esta linea
    //console.log(formData);

    const params = {
        method: "POST",
        headers: {
            Authorization: `Bearer ${getTokenApi()}`
        },
        body: formData
    };

    return fetch(url, params)
        .then(response => {
            return response.js //Aqui nos llega el header
        })
        .then(result => {
            return result; //Aqui nos llega el resultado decodificado
        })
        .catch(err => {
            return err;
        });
}

export function updateInfoApi(data) {
    const url = `${API_HOST}/modificarPerfil`;

    const params = {
        method: "PUT",
        headers: {
            Authorization: `Bearer ${getTokenApi()}`
        },
        body: JSON.stringify(data)
    };
    
    return fetch(url, params)
        .then(response => {
            return response;
        })
        .catch(err => {
            return err;    
        });  
}