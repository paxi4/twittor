import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import LeftMenu from '../../components/LeftMenu';

import "./BasicLayout.scss";

export default function BasicLayout(props) {
  
  const { className, children, setRefreshCheckLogin } = props;  // el setRefreshCheckLogin proviene del Home.js
  //console.log(props); //Este console nos ayuda a entender que cuando se ingresa al home, tambien se activa el children del "BasicLayout" || cuando en el home se le agrega el className en los props aparece ese dato 
  
  return (
    <Container className={`basic-layout ${className}`}> {/*Se agrega este className para saber en que pagina estamos, en la consola se mostrara como: basic-layout home container */}
        <Row>
            <Col xs={3} className="basic-layout__menu">
                <LeftMenu setRefreshCheckLogin={setRefreshCheckLogin}/>
            </Col>
            <Col xs={9} className="basic-layout__content">
                {children}
            </Col>
        </Row>
    </Container>
  )
}
