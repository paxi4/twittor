/* eslint-disable import/no-anonymous-default-export */
import Home from "../page/Home";
import Error404 from "../page/Error404";
import User from "../page/User";
import Users from "../page/Users";
export default [
    {   //esto en un enrutamiento tradicional por lo tanto va primero
        path:"/users",
        exact: true,
        page: Users
    },
    {    //esto es un enrutamiento dinamico por lo tanto va al final
        path:"/:id",
        exact: true,
        page: User
    },
    {
        path: "/", //esto es basicamente como esto: http://localhost:3000/ || es la pagina principal cuando estas logeado
        exact: true, //esto verfica si estas logeado
        page: Home //Aqui el "Home" esta relacionandolo con el, import Home from "../page/Home"
    },
    {
        path: "*", //esto es basicamente como esto: http://localhost:3000/nose/nose || es la pagina de errores || el "*" le esta diciendo cualquier pagina que no sea "/"
        page: Error404 //Aqui el "Home" esta relacionandolo con el, import Error404 from "../page/Error404"
    }
];