import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import {map} from "lodash"; //el map hace un bucle o un for
import configRouting from './configRouting';

export default function Routing(props) {
  
  const {setRefreshCheckLogin} = props; // el setRefreshCheckLogin proviene del App.js
  
  //El react-router-dom cambia mucho la sintaxis dependiendo de que version estes usando del mio es la v6: https://reactrouter.com/docs/en/v6/getting-started/overview || la version 5: https://v5.reactrouter.com/web/guides/quick-start
  return (
    <BrowserRouter>
        <Routes>
            {map(configRouting, (route, index) => ( //el index es la iteracion o posicion del route, como el 1 es el primero de la route "/"(home), el 2 de la route es "*"(error404). || el parentesis() rosa se pone en vez de {} porque el parentesis ya retorna viene con return.
                <Route key={index} path={route.path} exact={route.exact} element={<route.page setRefreshCheckLogin={setRefreshCheckLogin} />} />          
            ))}
        </Routes>
    </BrowserRouter>
  )
}
