import { createContext } from "react"; //el createContext se usa para crear un contexto

//Este Context contiene el token del usuario, basicamente todos sus datos: nombre, apellido, etc.
export const AuthContext = createContext(); //el createContext se usa para cuando quieres usar un dato de forma global sin tener que estar pasando de un componente a otro.